if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin()

Plug 'tpope/vim-sensible'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-rake'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-fugitive'
Plug 'crusoexia/vim-monokai'
Plug 'crusoexia/vim-javascript'
Plug 'crusoexia/vim-javascript-lib'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'scrooloose/nerdtree'
Plug 'vim-scripts/taglist.vim'
Plug 'christoomey/vim-tmux-navigator'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'mkitt/tabline.vim'
Plug 'edkolev/tmuxline.vim'
Plug 'vim-scripts/cscope.vim'
Plug 'scrooloose/syntastic'
Plug 'Valloric/YouCompleteMe'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'jiangmiao/auto-pairs'
Plug 'mxw/vim-jsx'
Plug 'pangloss/vim-javascript'
Plug 'mattn/emmet-vim'
Plug 'alvan/vim-closetag'
Plug 'mmozuras/snipmate-mocha'
Plug 'scrooloose/nerdcommenter'
Plug 'airblade/vim-gitgutter'
Plug 'rking/ag.vim'

" All of your Plugins must be added before the following line
call plug#end()

" Turn on syntax highlighting
syntax on

" Turn on backup copy
set backupcopy=yes

" Highlight and indent JSX in .js files
let g:jsx_ext_required = 0

" Leader key
let mapleader = ","

" map <leader>b and <leader>w to buffer prev/next buffer
noremap <leader>b :bp<CR>
noremap <leader>w :bn<CR>
noremap <leader>e :bd<CR>

" Close tags
let g:closetag_filenames = "*.html,*.xhtml,*.js,*.jsx,*.hbs"

" Tern config
let g:tern_show_argument_hints='on_hold'
let g:tern_map_keys=1

" Ultisnips
" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<c-j>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"

" NERDTree
nnoremap <F12> :NERDTreeToggle<CR>  
let NERDTreeShowLineNumbers = 1
let g:NERDTreeWinSize=50
autocmd FileType nerdtree setlocal relativenumber

let g:ctrlp_working_path_mode = 0

" Switch buffers
nnoremap <F5> :buffers<CR>:buffer<Space>

" Ctrl-P Ignore
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.lst,*.o,tags
set wildignore+=*/tmp/*,*.o,*.so,*.swp,*.zip,*/node_modules/*,*/bower_components/*
set wildignore+=*/build/*
set wildignore+=.dep

" Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_javascript_checkers = ['eslint']

" YouCompleteMe
let g:ycm_server_python_interpreter = '/usr/bin/python'
let g:ycm_collect_identifiers_from_tags_files = 1
let g:ycm_add_preview_to_completeopt=0
let g:ycm_confirm_extra_conf=0

" Remap escape key (does not work in paste mode)
inoremap jk <Esc>

" Security
set modelines=0

" Show relative line numbers
set relativenumber
set number

" Show file stats
set ruler

" Blink cursor on error instead of beeping (grr)
set visualbell

" Encoding
set encoding=utf-8

" Whitespace
"set wrap
set textwidth=79
set formatoptions=cqrn1
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set noshiftround

" Cursor motion
set scrolloff=3
set backspace=indent,eol,start
set matchpairs+=<:> " use % to jump between pairs
runtime! macros/matchit.vim

" Move up/down editor lines
nnoremap j gj
nnoremap k gk

" Confirm unsaved buffers
set confirm
set hidden

" Rendering
set ttyfast

" Status bar
set laststatus=2

" Last line
set showmode
set showcmd

" Close preview window when done
autocmd CompleteDone * pclose

" Searching
nnoremap / /\v
vnoremap / /\v
set hlsearch
set incsearch
set ignorecase
set smartcase
set showmatch
map <leader><space> :let @/=''<cr> " clear search

" vimrc editing
map <F6> :so ~/.vimrc<CR>
map <F9> :e ~/.vimrc<CR>

nnoremap <silent> <F11> :TlistUpdate<CR> :TlistToggle<CR>
let Tlist_GainFocus_On_ToggleOpen = 1
let Tlist_WinWidth = 50

" Search and replace word under cursor
nnoremap <F10> :%s/<c-r><c-w>/<c-r><c-w>/gc<c-f>$F/i

" paste toggle
set pastetoggle=<f8>

" Textmate holdouts

" Formatting
map <leader>q gqip

" Visualize tabs and newlines
set listchars=tab:▸\ ,eol:¬
" Uncomment this to enable by default:
set list " To enable by default
" Or use your leader key + l to toggle on/off
map <leader>l :set list!<CR> " Toggle tabs and EOL

let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1

" Color scheme (terminal)
set t_Co=256
set background=dark
"let g:solarized_termcolors=256
"let g:solarized_termtrans=1
" put https://raw.github.com/altercation/vim-colors-solarized/master/colors/solarized.vim
" in ~/.vim/colors/ and uncomment:
colorscheme monokai
